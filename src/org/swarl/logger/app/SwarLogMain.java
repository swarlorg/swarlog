/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.logger.app;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.swarl.brack11.adif2.Adif2Reader;
import org.swarl.brack11.adif2.Adif2Writer;
import org.swarl.brack11.adif2.enums.Mode;
import org.swarl.brack11.adif2.enums.QSLRcvd;
import org.swarl.brack11.adif2.model.Adif2Record;
import org.swarl.brack11.prefixes.PrefixList;
import org.swarl.logger.app.model.EQslHelper;
import org.swarl.logger.app.model.SwarLogPreferences;
import org.swarl.logger.app.utils.Alerts;
import org.swarl.logger.app.view.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.Optional;
import java.util.prefs.Preferences;

public class SwarLogMain extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;

    private ObservableList<Adif2Record> records = FXCollections.observableArrayList();
//    private ObservableList<Prefix> prefixes = FXCollections.observableArrayList();
    private PrefixList prefixes;
    private Adif2Record lastRecord;

    private SwarLogPreferences preferences;
    private File prefFile = new File(System.getProperty("user.dir") + File.separator + ".preferences.xml");

//    public int stageWidth, stageHeight;

    @Override
    public void start(Stage primaryStage){
        primaryStage.setMaximized(false);

        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("SWARLog");

        initRootLayout();
        showLogger();
    }

    public ObservableList<Adif2Record> getRecords() {
        return records;
    }

    public PrefixList getPrefixes(){
        return prefixes;
    }

    public void setPrefixes(){
        PrefixList prefixList = new PrefixList();
        prefixes = prefixList;
    }

    /**
     * Initiate root layout
     */
    public void initRootLayout(){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/RootLayout.fxml"));
            rootLayout = loader.load();
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);

            RootLayoutController controller = loader.getController();
            controller.setMainApp(this);
            if (preferencesExist()) {
                controller.setPreferences(preferences);
            }

            primaryStage.show();
        } catch (IOException e) {
            Alerts.exceptionAlert(e, "Exception thrown: ");
        }

        setPrefixes();

        // try to load last open file
        File file = getAdifFilePath();
        if(file != null && file.exists()) {
            loadDataFromFile(file);
        } else {
            writeChangesTo(new File(System.getProperty("user.dir") + File.separator + "swarlog.adi"));
        }
    }

    /**
     * showing the main window of the logger
     *
     */
    public void showLogger(){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/LoggerLayout.fxml"));
            AnchorPane loggerWindow = loader.load();
            rootLayout.setCenter(loggerWindow);

            LoggerLayoutController controller = loader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            Alerts.exceptionAlert(e, "Exception thrown: ");
        }

    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }


    public void loadDataFromFile(File file) {
        try {
            Adif2Reader adifReader = new Adif2Reader(file.getPath(),records);

            if(records != null) records.clear();
            adifReader.read();
            records = adifReader.getRecords();
            records.sort(compareByDayDesc);
            lastRecord = records.size() > 0 ? records.get(0) : null;

            setAdifFilePath(file);

        } catch (IOException e) {
            Alerts.exceptionAlert(e, "Exception thrown: ");
        }
    }

    public void importDataFromFile(File file) {
        try {
            ObservableList<Adif2Record> importedRecords = FXCollections.observableArrayList();
            Adif2Reader adif2Reader = new Adif2Reader(file.getPath(),importedRecords);
            if (importedRecords != null) importedRecords.clear();
            adif2Reader.read();
            importedRecords = adif2Reader.getRecords();
            records.addAll(importedRecords);
//            records.sort(compareByDayDesc);
            writeChangesTo();
        } catch (IOException e) {
            Alerts.exceptionAlert(e, "");
        }
    }

    public void saveToAdiFile() {
        Optional<ButtonType> result = Alerts.confirmationAlert("Save file", "Confirm Saving File", "Confirm saving process which ovverride file content. Changes cannot be reverted");
        if (!result.get().equals(ButtonType.CANCEL)) {
            writeChangesTo();
        }
    }

    public void writeChangesTo() {
        File adifFile = getAdifFilePath();
        Adif2Writer writer = new Adif2Writer();
        writer.newFile(adifFile);
        records.sort(compareByDayAsc);
        for (Adif2Record record : records) {
            writer.write(record,adifFile.getPath());
        }
        records.sort(compareByDayDesc);
    }

    public void writeChangesTo(File adifFile) {
        Adif2Writer writer = new Adif2Writer();
        writer.newFile(adifFile);
        records.sort(compareByDayAsc);
        for (Adif2Record record : records) {
            writer.write(record,adifFile.getPath());
        }
        records.sort(compareByDayDesc);
        setAdifFilePath(adifFile);
    }

    public File getAdifFilePath() {
        Preferences prefs = Preferences.userNodeForPackage(SwarLogMain.class);
        String filePath = prefs.get("filePath",null);
        if (filePath != null) {
            return new File(filePath);
        } else {
            return null;
        }
    }

    public void setAdifFilePath(File file) {
        Preferences prefs = Preferences.userNodeForPackage(SwarLogMain.class);
        if(file != null) {
            prefs.put("filePath",file.getPath());

            // Renew headlines of scene
            primaryStage.setTitle("Logger - "+file.getPath());
            // primaryStage.setMaximized(true);
        } else {
            prefs.remove("Logger");
        }
    }

    public void eQslSend() {
        Optional<ButtonType> result = Alerts.confirmationAlert("Send Info", null, "New records will be sent to eQSL.cc");

        if (!result.get().equals(ButtonType.CANCEL)) {
            EQslHelper eQslHelper = new EQslHelper("http://www.eqsl.cc/qslcard");
            try {
                if (preferencesExist()
                        && (preferences.getMyEQslCall() != null)
                        && (preferences.getMyEQslPass() != null)
                        && (preferences.getMyEQslCall().length() > 0)
                        && (preferences.getMyEQslPass().length() > 0)) {
                    eQslHelper.setPreferences(preferences);
                    File adifFile = EQslHelper.createTmpFile();
                    Adif2Writer writer = new Adif2Writer();
                    int count = 0;

                    for (Adif2Record record : records) {
                        if (record.getQslSentVia() == null) {
                            count++;
                            writer.write(record, adifFile.getPath());
                        }
                    }

                    if (count == 0) {
                        Alerts.errorAlert("No Records", "No valid records found", "There are no new logs that can be uploaded.");
                      } else {
                        if (eQslHelper.deliverLog(adifFile)) {
                            ObservableList<Adif2Record> newRecords = FXCollections.observableArrayList();
                            for (Adif2Record record : records) {
                                if (record.getQslSentVia() == null) {
                                    record.setEqslQslSent("Y");
                                    record.setQslSentVia("E");
                                }
                                newRecords.add(record);
                            }
                            records = newRecords;
                            writeChangesTo();

                            Alerts.infoAlert("Success!", null, "Your log is successfully uploaded to eQSL.cc");
                        }
                    }
                } else {
                    Alerts.warningAlert("eQSL.cc connect", "Cannot connect to eQSL.cc", "Before using this option \nyou have to insert proper eQSL.cc login information \nin preferences menu and restart the program.");
                }
            } catch (IOException e) {
                Alerts.exceptionAlert(e, "Exception thrown: ");
            }
        }
    }

    public void eQslReceive() throws Exception {
        Optional<ButtonType> result = Alerts.confirmationAlert("Send Info", null, "New confirmed QSOs will be downloaded from your eQSL.cc inbox");
        ObservableList<Adif2Record> rcvdAdif = FXCollections.observableArrayList();
        if (result.get().equals(ButtonType.OK)) {
            EQslHelper eQslHelper = new EQslHelper("http://www.eqsl.cc/qslcard");
            if (preferencesExist()
                    && (preferences.getMyEQslCall() != null)
                    && (preferences.getMyEQslPass() != null)
                    && (preferences.getMyEQslCall().length() > 0)
                    && (preferences.getMyEQslPass().length() > 0)) {
                eQslHelper.setPreferences(preferences);
                File rcvdQsls = eQslHelper.eQslReceive(); //downloading adi file
                if (rcvdQsls != null) {
                    Adif2Reader reader = new Adif2Reader(rcvdQsls.getPath(), rcvdAdif); //reading downloaded adi file
                    reader.read(); //converting read file into ObservableList

                    Optional<ButtonType> choice = Alerts.confirmationAlert("New eQSLs", "You have received " + rcvdAdif.size() + " new eQSLs", "would you like to update " + rcvdAdif.size() + "  your log records?");
                    if (choice.get().equals(ButtonType.OK)) {
                        ObservableList<Adif2Record> newRecords = FXCollections.observableArrayList();
                        for (Adif2Record record : records) {
                            for (Adif2Record rcvdRecord : rcvdAdif) {
                                if (record.getCall().equalsIgnoreCase(rcvdRecord.getCall())
                                        && record.getQsoDate().equals(rcvdRecord.getQsoDate())
                                        && record.getTimeOn().equals(rcvdRecord.getTimeOn())) {
                                    record.setQsoDate(rcvdRecord.getQsoDate());
                                    record.setTimeOn(rcvdRecord.getTimeOn());
                                    record.setEqslQslRcvd(QSLRcvd.YES);
                                }
                            }
                            newRecords.add(record);
                        }
                        records = newRecords;
                        writeChangesTo();
                    }
                } else {
                    Alerts.warningAlert("", "", "No new QSLs received since last update.");
                }
            } else {
                Alerts.warningAlert("eQSL.cc connect", "Cannot connect to eQSL.cc", "Before using this option \nyou have to insert proper eQSL.cc login information \nin preferences menu and restart the program.");
            }
        }
    }


    public void showPreferencesDialog() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/PreferencesEditor.fxml"));
            AnchorPane preferencesWindow = loader.load();

            // Create dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("SWL Personal preferences");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initStyle(StageStyle.UNDECORATED);
            //dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(preferencesWindow);
            dialogStage.setScene(scene);

            PreferencesEditorController controller = loader.getController();
            controller.setMainApp(this);
            if (preferencesExist()) {
                controller.setPreferences(getPreferences());
            } else {
                controller.setPreferences(new SwarLogPreferences());
            }

            controller.setDialogStage(dialogStage);

            dialogStage.show();

        } catch (Exception e) {
            Alerts.exceptionAlert(e, "Exception thrown: ");
        }
    }


    public void showAddLogDialog() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/AddLogRecord.fxml"));
            AnchorPane addLogWindow = loader.load();

            // Create dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Add Log Record");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            //dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(addLogWindow);
            dialogStage.setScene(scene);

            AddLogRecordController controller = loader.getController();
            controller.initialize(this, lastRecord, false);
            controller.setDialogStage(dialogStage);

            dialogStage.show();

        } catch (Exception e) {
            Alerts.exceptionAlert(e, "Exception thrown: ");
        }
    }


    public void showAddLogDialog(Adif2Record record) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/AddLogRecord.fxml"));
            AnchorPane addLogWindow = loader.load();

            // Create dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Add Log Record");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            //dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(addLogWindow);
            dialogStage.setScene(scene);

            AddLogRecordController controller = loader.getController();
            controller.initialize(this, record, true);
            controller.setDialogStage(dialogStage);

            dialogStage.show();

        } catch (Exception e) {
            Alerts.exceptionAlert(e, "Exception thrown: ");
        }
    }


    public void showEditDialog(Adif2Record selectedRecord) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/EditLogRecord.fxml"));
            AnchorPane editLogWindow = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Editing: " + selectedRecord.getCall());
            dialogStage.initModality(Modality.WINDOW_MODAL);
            //dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(editLogWindow);
            dialogStage.setScene(scene);

            EditLogRecordController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);
            controller.loadRecord(selectedRecord);

            dialogStage.show();
        } catch (IOException e) {
            Alerts.exceptionAlert(e, "Exception thrown: ");
        }
    }


    private boolean preferencesExist() {
        if (prefFile.exists() && prefFile.isFile()) {
            try {
                JAXBContext context = JAXBContext.newInstance(SwarLogPreferences.class);
                Unmarshaller um = context.createUnmarshaller();
                preferences = (SwarLogPreferences) um.unmarshal(prefFile);
                return true;
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }


    public SwarLogPreferences getPreferences() {
        return preferences;
    }


    public void savePreferences(SwarLogPreferences preferences) {
        try {
            JAXBContext context = JAXBContext.newInstance(preferences.getClass());
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            m.marshal(preferences, prefFile);
            //Alerts.errorAlert(null, null, System.getProperty("user.home"));
        } catch (Exception e) {
            Alerts.exceptionAlert(e, "Cannot save preferences: ");
        }
    }


    public void showPrefixesDialog() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/Prefixes.fxml"));

            GridPane prefixesDialog = loader.load();

            // Create dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("World Prefixes");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            //dialogStage.initOwner(primaryStage);
            dialogStage.setWidth(1000);
            Scene scene = new Scene(prefixesDialog);
            dialogStage.setScene(scene);

            PrefixesController controller = loader.getController();
            controller.setMainApp(this);

            controller.setDialogStage(dialogStage);

            dialogStage.show();
        } catch (IOException e) {
            Alerts.exceptionAlert(e, "Exception thrown: ");
        }
    }

    /**
     * Opens specified webpage in default browser
     * @param url
     */
    public void showWeb(String url) {
       getHostServices().showDocument(url);
    }


    public EnumMap<Mode, Boolean> readUsedModes() {
        EnumMap<Mode, Boolean> visibleModes = new EnumMap<>(Mode.class);
        for (Mode m : Mode.values()) {
            visibleModes.put(m, true);
        }
        //ObservableList<Mode> modesUsed = FXCollections.observableArrayList(visibleModes);
        File fi = new File(System.getProperty("user.dir") + File.separator + ".usedmodes.ser");
        if (fi.exists() && fi.isFile()) {
            try {
                FileInputStream fis = new FileInputStream(fi);
                ObjectInputStream ois = new ObjectInputStream(fis);
                visibleModes = (EnumMap<Mode, Boolean>) ois.readObject();
                ois.close();
            } catch (Exception e) {
                Alerts.exceptionAlert(e,"Cannot write to .usedmodes.ser file");
            }
        }
        return visibleModes;
    }

    public void writeUsedModes(EnumMap<Mode, Boolean> visibleModes) {
        //EnumMap<Mode, Boolean> visibleModes = new EnumMap<Mode, Boolean>(observableMap);
        File fo = new File(System.getProperty("user.dir") + File.separator + ".usedmodes.ser");

        try {
            if (!fo.exists()) fo.createNewFile();
            FileOutputStream fos = new FileOutputStream(fo);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(visibleModes);
            oos.close();
        } catch (Exception e) {
            Alerts.exceptionAlert(e, "Cannot open .usedmodes.ser file.");
        }
    }

    /**
     * Comparator for sorting the log tableview by date DESC
     */
    Comparator<Adif2Record> compareByDayDesc = new Comparator<Adif2Record>() {
        @Override
        public int compare(Adif2Record o1, Adif2Record o2) {
            return (o2.getQsoDate().toString() + o2.getTimeOn().toString()).compareToIgnoreCase((o1.getQsoDate().toString()+ o1.getTimeOn().toString()));
        }
    };


    /**
     * Comparator for sorting the log tableview by date Asc
     */
    Comparator<Adif2Record> compareByDayAsc = new Comparator<Adif2Record>() {
        @Override
        public int compare(Adif2Record o1, Adif2Record o2) {
            return (o1.getQsoDate().toString() + o1.getTimeOn().toString()).compareToIgnoreCase((o2.getQsoDate().toString()+ o2.getTimeOn().toString()));
        }
    };


    public static void main(String[] args) {
        launch(args);
    }

}
