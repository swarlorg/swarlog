/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.logger.app.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by brack on 2017-03-22.
 */
@XmlRootElement(name = "preferences")
public class SwarLogPreferences {

    private String myCall;
    private String myName;
    private String myEQslCall;
    private String myEQslPass;
    private String myEQslMessage = "PLEASE CONFIRM YOUR QSO WITH:";
    private String myLat;
    private String myLon;

    @XmlElement(name = "myCall")
    public String getMyCall() {
        return myCall;
    }

    public void setMyCall(String myCall) {
        this.myCall = myCall;
    }

    @XmlElement(name = "myName")
    public String getMyName() {
        return myName;
    }

    public void setMyName(String myName) {
        this.myName = myName;
    }

    @XmlElement(name = "myEQslCall")
    public String getMyEQslCall() {
        return myEQslCall;
    }

    public void setMyEQslCall(String myEQslCall) {
        this.myEQslCall = myEQslCall;
    }

    @XmlElement(name = "myEQslPass")
    public String getMyEQslPass() {
        return myEQslPass;
    }

    public void setMyEQslPass(String myEQslPass) {
        this.myEQslPass = myEQslPass;
    }

    @XmlElement(name = "myEQslMessage")
    public String getMyEQslMessage() {
        return myEQslMessage;
    }

    public void setMyEQslMessage(String myEQslMessage) {
        this.myEQslMessage = myEQslMessage;
    }

    @XmlElement(name = "myLat")
    public String getMyLat() {
        return myLat;
    }

    public void setMyLat(String myLat) {
        this.myLat = myLat;
    }

    @XmlElement(name = "myLon")
    public String getMyLon() {
        return myLon;
    }

    public void setMyLon(String myLon) {
        this.myLon = myLon;
    }
}
