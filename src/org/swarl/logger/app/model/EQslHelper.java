/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.logger.app.model;


import okhttp3.*;
import org.swarl.logger.app.utils.Alerts;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EQslHelper {
    private static SwarLogPreferences preferences;
    private HttpUrl baseUrl;

    public EQslHelper(String baseUrl) {
        preferences = new SwarLogPreferences();
        this.baseUrl  = HttpUrl.parse(baseUrl);
    }

    public static File createTmpFile() throws IOException {
        Path path = Files.createTempFile("SWARLog-", ".adi");
        File file = path.toFile();
        Files.write(path, buildHeader().getBytes(StandardCharsets.UTF_8));
        file.deleteOnExit();
        return file;
    }

    public static File createTmpFile(String prefix, String suffix) throws IOException {
        Path path = Files.createTempFile(prefix, suffix);
        File file = path.toFile();
        file.deleteOnExit();
        return file;
    }

    public void setPreferences(SwarLogPreferences preferences) {
        EQslHelper.preferences = preferences;
    }

    public File eQslReceive() throws Exception {
        String lastUploadDate = lastDateOfUpload().format(DateTimeFormatter.ofPattern("yyyyMMddHHmm"));
        OkHttpClient client = new OkHttpClient();
        HttpUrl url = baseUrl.newBuilder()
                .addPathSegment("DownloadInBox.cfm")
                .addQueryParameter("username", preferences.getMyEQslCall())
                .addQueryParameter("password", preferences.getMyEQslPass())
                .addQueryParameter("ConfirmedOnly", "1")
                .addQueryParameter("RcvdSince", lastUploadDate)
                .build();

        Request request = new Request.Builder()
                .url(url.toString())
                .build();

        Response response = client
                .newCall(request)
                .execute();

        ResponseBody body = response
                .body();

        String bodyStr = body.string();

        response.close();
        //System.out.println(bodyStr);
        if (foundConfirmed(bodyStr)) {
            return downloadAdif(bodyStr);
        }
        return null;
    }

    public boolean deliverLog(File adifFile) {
        try {
            OkHttpClient client = new OkHttpClient();

            HttpUrl url = baseUrl.newBuilder()
                    .addPathSegment("ImportADIF.cfm")
                    .build();

            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("Filename", adifFile.getName(),
                            RequestBody.create(MediaType.parse("text/plain"), adifFile))
                    .build();

            Request request = new Request.Builder().url(url.toString()).post(formBody).build();
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                response.close();
                return adifFile.delete();
            } else {
                Alerts.errorAlert("ERROR!", null, "eQSL.cc gave the following reply:\n" + response.message());
            }
        } catch (IOException e) {
            Alerts.exceptionAlert(e, "Exception thrown: ");
        }
        return false;
    }

    public void setBaseUrl(String url) {
        baseUrl = HttpUrl.parse(url);
    }

    private static String buildHeader() {
        String user = preferences.getMyEQslCall();
        String pswd = preferences.getMyEQslPass();
        StringBuilder sb = new StringBuilder();
        sb.append("ADIF 2 Export from SWARLOG\n");
        sb.append("Generated on " + LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE) + " " + LocalTime.now().format(DateTimeFormatter.ISO_TIME) + "\n");
        sb.append("<PROGRAMID:7>SWARLOG\n");
        sb.append("<ADIF_Ver:1>2\n");
        if ((user != null) || (pswd != null)) {
            sb.append("<EQSL_USER:" + user.length() + ">" + user + "\n");
            sb.append("<EQSL_PSWD:" + pswd.length() + ">" + pswd + "\n");
        }
        sb.append("<EOH>\n");

        return sb.toString();

    }

    private LocalDateTime lastDateOfUpload() {
        LocalDate ld = LocalDate.now(ZoneId.of("UTC"));
        LocalTime lt = LocalTime.now(ZoneId.of("UTC"));
        try {
            OkHttpClient client = new OkHttpClient();

            HttpUrl url = baseUrl.newBuilder()
                    .addPathSegment("DisplayLastUploadDate.cfm")
                    .addQueryParameter("username", preferences.getMyEQslCall())
                    .addQueryParameter("password", preferences.getMyEQslPass())
                    .build();

            Request request = new Request.Builder()
                    .url(url.toString())
                    .build();

            Response response = client.newCall(request).execute();

            ResponseBody body = response.body();

            String dt = body.string();
            response.close();
            Pattern p = Pattern.compile("([0-9]+\\-[A-z]+\\-[0-9]+)+[\\sat]+([0-9]+:[0-9]+)");
            Matcher m = p.matcher(dt);
            if (m.find()) {
                ld = LocalDate.parse(m.group(1), DateTimeFormatter.ofPattern("dd-MMM-yyyy", Locale.ENGLISH));
                lt = LocalTime.parse(m.group(2), DateTimeFormatter.ofPattern("HH:mm", Locale.ENGLISH));
            }
        } catch (IOException e) {
            Alerts.exceptionAlert(e, "Exception thrown: ");
        }
        LocalDateTime ldt = LocalDateTime.of(ld,lt);
        LocalDateTime result = ldt.minusMonths(3);
        return result;
    }

    private File downloadAdif(String bodyStr) throws IOException {
        File destinationFile = createTmpFile("eQSL-", ".adi");
        String adifName = getAdifName(bodyStr);
        OkHttpClient client = new OkHttpClient();
        HttpUrl url = baseUrl.newBuilder()
                .addPathSegment("downloadedfiles")
                .addPathSegment(adifName)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("Failed to download file: " + response);
        }
        FileOutputStream fos = new FileOutputStream(destinationFile);
        fos.write(response.body().bytes());
        fos.close();
        return destinationFile;
    }

    private String getAdifName(String str) {
        String resultStr = "";
        Pattern p = Pattern.compile("\\w+?\\.adi");
        Matcher m = p.matcher(str);
        if (m.find()) {
            resultStr = m.group(0);
        } else {
            Alerts.errorAlert("Error", null, "Coud not find adif file");
        }
        return resultStr;
    }

    private boolean foundConfirmed(String str) {
        String search = "Your ADIF log file has been built";
        return str.toLowerCase().contains(search.toLowerCase());
    }

}
