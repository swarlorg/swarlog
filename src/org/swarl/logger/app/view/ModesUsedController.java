/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.logger.app.view;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.util.Callback;
import org.swarl.brack11.adif2.enums.Mode;
import org.swarl.logger.app.SwarLogMain;

import java.util.EnumMap;
import java.util.Map;

public class ModesUsedController {
    private SwarLogMain mainApp;
    private EnumMap<Mode, Boolean> modesUsed;

    @FXML
    private TableView<Map.Entry<Mode, Boolean>> modesUsedTable;

    @FXML
    private TableColumn<Map.Entry<Mode, Boolean>, String> modeNameColumn;

    @FXML
    private TableColumn<Map.Entry<Mode, Boolean>, Boolean> modeUsedColumn;

    @FXML
    private void initialize() {
        modeNameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Map.Entry<Mode, Boolean>, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Map.Entry<Mode, Boolean>, String> param) {
                Mode mode = param.getValue().getKey();
                if (Mode.isRoot(mode)) {
                    return new SimpleStringProperty(mode.toString());
                } else {
                    return new SimpleStringProperty("    " + mode.toString());
                }
            }
        });
        modeNameColumn.setSortable(false);
        modeUsedColumn.setCellValueFactory(c -> {
            Mode selectedMode = c.getValue().getKey();
            BooleanProperty selectedUsedProperty = new SimpleBooleanProperty(c.getValue().getValue());
            selectedUsedProperty.addListener((observable, oldValue, newValue) -> {
                c.getValue().setValue(newValue);
                if (selectedMode.hasChildren()) {
                    for (Map.Entry<Mode, Boolean> entry : modesUsed.entrySet()) {
                        Mode child = entry.getKey();
                        if (child.parent() != null && child.parent().equals(selectedMode)) {
                            entry.setValue(newValue);
                        }
                    }
                }
                if (!Mode.isRoot(selectedMode)) {
                    modesUsed.replace(selectedMode.parent(), true);
                }
                modesUsedTable.refresh();
                mainApp.writeUsedModes(modesUsed);
            });
            return selectedUsedProperty;
        });
        modeUsedColumn.setCellFactory(c -> new CheckBoxTableCell<>());
    }

    private void configureTable() {
        modesUsed = mainApp.readUsedModes();
        ObservableList<Map.Entry<Mode, Boolean>> items = FXCollections.observableArrayList(modesUsed.entrySet());
        modesUsedTable.setItems(items);
        modesUsedTable.setEditable(true);
    }

    public void setMainApp(SwarLogMain mainApp) {
        this.mainApp = mainApp;
        configureTable();
    }
}
