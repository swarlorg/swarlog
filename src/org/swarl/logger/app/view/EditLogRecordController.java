/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.logger.app.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.swarl.brack11.adif2.enums.Mode;
import org.swarl.brack11.adif2.enums.QSLRcvd;
import org.swarl.brack11.adif2.model.Adif2Record;
import org.swarl.brack11.prefixes.PrefixList;
import org.swarl.brack11.prefixes.enums.PrefixKind;
import org.swarl.brack11.prefixes.model.Prefix;
import org.swarl.logger.app.SwarLogMain;
import org.swarl.logger.app.utils.Alerts;

import java.time.LocalDate;
import java.time.ZoneId;

public class EditLogRecordController {

    @FXML
    private DatePicker datePicker;

    @FXML
    private ComboBox<Mode> modeChoiceBox;

    @FXML
    private TextField freqField;

    @FXML
    private TextField rstField;

    @FXML
    private ChoiceBox<QSLRcvd> eQslChoiceBox;

    @FXML
    private ChoiceBox<QSLRcvd> qslRcvdChoiceBox;

    @FXML
    private TextField latField;

    @FXML
    private TextField lonField;

    @FXML
    private TextField gridField;

    @FXML
    private ChoiceBox<Prefix> countryChoiceBox;

    @FXML
    private TextField territoryField;

    @FXML
    private TextField cqField;

    @FXML
    private TextField ituField;

    @FXML
    private TextField iotaField;

    @FXML
    private TextField callField;

    @FXML
    private TextArea addressField;

    @FXML
    private TextArea qslMessageField;

    @FXML
    private TextField noteField;

    private SwarLogMain mainApp;
    private ObservableList<Prefix> countries = FXCollections.observableArrayList();
    private Adif2Record record;
    private Stage dialogStage;


    @FXML
    private void initialize() {
        countries = PrefixList.getRecords(new PrefixKind[]{PrefixKind.DXCC,PrefixKind.NONDXCC});
        ObservableList<Mode> modes = FXCollections.observableArrayList(Mode.values());
        modeChoiceBox.getItems().addAll(modes);
        modeChoiceBox.setCellFactory(new Callback<ListView<Mode>, ListCell<Mode>>() {
            @Override
            public ListCell<Mode> call(ListView<Mode> param) {
                return new ListCell<Mode>() {
                    @Override
                    protected void updateItem(Mode item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setGraphic(null);
                    } else {
                        if (Mode.isRoot(item)) {
                            setTextFill(Color.BLACK);
                            setText(item.toString());
                        } else {
                            setTextFill(Color.BLUE);
                            setText("   " + item.toString());
                        }
                    }
                    }
                };
            }
        });
    }

    @FXML
    private void handleSubmit() {
        if (isValid()) {
            record.setQsoDate(datePicker.getValue());
            record.setMode(modeChoiceBox.getValue());
            record.setFreq(freqField.getText());
            record.setRstRcvd(rstField.getText());
            record.setEqslQslRcvd(eQslChoiceBox.getValue());
            record.setQslRcvd(qslRcvdChoiceBox.getValue());
            record.setLat(latField.getText());
            record.setLon(lonField.getText());
            if (gridField.getText() != null) record.setGridsquare(gridField.getText());
            record.setCountry(countryChoiceBox.getValue().getTerritory());
            if (territoryField.getText() != null) record.setAppSwarlogTerritory(territoryField.getText());
            if (cqField.getText() != null) record.setCqz(cqField.getText());
            if (ituField.getText() != null) record.setItuz(ituField.getText());
            if (iotaField.getText() != null) record.setIota(iotaField.getText());
            record.setCall(callField.getText());
            if (addressField.getText() != null) record.setAddress(addressField.getText());
            if (qslMessageField.getText() != null) record.setQslmsg(qslMessageField.getText());
            if (noteField.getText() != null) record.setNotes(noteField.getText());

            mainApp.saveToAdiFile();
            dialogStage.close();
        } else {
            Alerts.errorAlert(null,null, "All fields with colored background cannot be empty.");
        }
    }

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    public void setMainApp(SwarLogMain mainApp) {
        this.mainApp = mainApp;
    }

    public void loadRecord(Adif2Record r) {
        record = r;
        ObservableList<QSLRcvd> qslRcvds = FXCollections.observableArrayList(QSLRcvd.values());
        Prefix prefix = PrefixList.prefixByTerritory(countries, record.getCountry());
        String rstValue = (record.getRstRcvd() != null) ? record.getRstRcvd() : record.getRstSent();

        datePicker.setValue(record.getQsoDate() != null ? record.getQsoDate() : LocalDate.now(ZoneId.of("UTC")));
        modeChoiceBox.getSelectionModel().select(record.getMode());
        freqField.setText(record.getFreq() > 0 ? record.getFreq().toString() : "");
        rstField.setText(rstValue);
        eQslChoiceBox.setItems(qslRcvds);
        eQslChoiceBox.getSelectionModel().select(record.getEqslQslRcvd());
        qslRcvdChoiceBox.setItems(qslRcvds);
        qslRcvdChoiceBox.getSelectionModel().select(record.getQslRcvd());
        latField.setText(record.getLat() != null ? record.getLat().getCoordinate() : "0");
        lonField.setText(record.getLon() != null ? record.getLon().getCoordinate() : "0");
        gridField.setText(record.getGridsquare() != null ? record.getGridsquare() : "");
        countryChoiceBox.setItems(countries);
        countryChoiceBox.getSelectionModel().select(prefix);
        territoryField.setText(record.getAppSwarlogTerritory() != null ? record.getAppSwarlogTerritory() : "");
        cqField.setText(record.getCqz() != null ? record.getCqz() : "");
        ituField.setText(record.getItuz() != null ? record.getItuz() : "");
        iotaField.setText(record.getIota() != null ? record.getIota() : "");
        callField.setText(record.getCall() != null ? record.getCall() : "");
        addressField.setText(record.getAddress() != null ? record.getAddress() : "");
        qslMessageField.setText(record.getQslmsg() != null ? record.getQslmsg() : "");
        noteField.setText(record.getNotes() != null ? record.getNotes() : "");
    }

    private boolean isValid() {
        if (datePicker.getValue() == null) return false;
        if (modeChoiceBox.getValue() == null) return false;
        if (freqField.getText() == null || freqField.getText().length() == 0) return false;
        if (rstField.getText() == null || rstField.getText().length() == 0) return false;
        if (eQslChoiceBox.getValue() == null) return false;
        if (qslRcvdChoiceBox.getValue() == null) return false;
        if (latField.getText() == null || latField.getText().length() == 0) return false;
        if (lonField.getText() == null || lonField.getText().length() == 0) return false;
        if (countryChoiceBox.getValue().getTerritory() == null) return false;
        if (callField.getText() == null || callField.getText().length() == 0) return false;
        return true;
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
}
