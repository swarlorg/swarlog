<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ Licensed to the Apache Software Foundation (ASF) under one
  ~ or more contributor license agreements.  The ASF licenses this file
  ~ to you under the Apache License, Version 2.0 (the
  ~ License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~  http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  ~
  ~
  ~ Created by Yury Bondarenko
  ~ contact email this.brack@gmail.com
  ~
  -->

<?import javafx.geometry.Insets?>
<?import javafx.scene.control.*?>
<?import javafx.scene.image.*?>
<?import javafx.scene.layout.*?>
<?import javafx.scene.text.Font?>
<AnchorPane xmlns="http://javafx.com/javafx/8.0.141" xmlns:fx="http://javafx.com/fxml/1" fx:controller="org.swarl.logger.app.view.LoggerLayoutController">
    <children>
      <GridPane layoutY="10.0" prefHeight="50.0" AnchorPane.leftAnchor="10.0" AnchorPane.rightAnchor="10.0">
         <columnConstraints>
            <ColumnConstraints hgrow="SOMETIMES" maxWidth="556.0" minWidth="400.0" prefWidth="400.0" />
            <ColumnConstraints hgrow="ALWAYS" />
            <ColumnConstraints hgrow="SOMETIMES" maxWidth="427.0" minWidth="400.0" prefWidth="400.0" />
         </columnConstraints>
         <rowConstraints>
            <RowConstraints minHeight="10.0" vgrow="SOMETIMES" />
         </rowConstraints>
         <children>
            <HBox prefHeight="42.0" GridPane.valignment="CENTER">
               <children>
                    <ButtonBar buttonMinWidth="40.0" maxHeight="-Infinity" maxWidth="-Infinity" minHeight="40.0" prefHeight="40.0" prefWidth="40.0">
                        <buttons>
                            <Button alignment="CENTER" contentDisplay="GRAPHIC_ONLY" defaultButton="true" mnemonicParsing="false" onAction="#handleNewRecord" text="Add...">
                           <graphic>
                              <ImageView fitHeight="34.0" fitWidth="34.0" pickOnBounds="true" preserveRatio="true">
                                 <image>
                                    <Image url="@icons/new.png" />
                                 </image>
                              </ImageView>
                           </graphic>
                           <tooltip>
                              <Tooltip text="New QSO" />
                           </tooltip></Button>
                            <Button alignment="CENTER" contentDisplay="GRAPHIC_ONLY" layoutX="34.0" layoutY="34.0" mnemonicParsing="false" onAction="#handleDeleteRows" text="Detete selected">
                           <graphic>
                              <ImageView fitHeight="34.0" fitWidth="34.0" pickOnBounds="true" preserveRatio="true">
                                 <image>
                                    <Image url="@icons/delete.png" />
                                 </image>
                              </ImageView>
                           </graphic>
                           <tooltip>
                              <Tooltip text="Delete selected record(s)" />
                           </tooltip></Button>
                        </buttons>
                    </ButtonBar>
               </children>
            </HBox>
            <HBox alignment="TOP_RIGHT" prefHeight="42.0" GridPane.columnIndex="2" GridPane.valignment="CENTER">
               <children>
                  <ButtonBar buttonMinWidth="40.0" maxHeight="-Infinity" maxWidth="-Infinity" minHeight="40.0" prefHeight="40.0" prefWidth="40.0">
                     <buttons>
                        <Button alignment="CENTER" contentDisplay="GRAPHIC_ONLY" maxHeight="-Infinity" maxWidth="-Infinity" mnemonicParsing="false" onAction="#handlePreferences" prefWidth="42.0" text="Preferences">
                           <graphic>
                              <ImageView fitHeight="34.0" fitWidth="34.0" pickOnBounds="true" preserveRatio="true">
                                 <image>
                                    <Image url="@icons/settings.png" />
                                 </image>
                              </ImageView>
                           </graphic>
                           <tooltip>
                              <Tooltip text="Preferences" />
                           </tooltip></Button>
                        <Button alignment="CENTER" contentDisplay="CENTER" mnemonicParsing="false" onAction="#handleDxCluster" text="DX Cluster">
                           <graphic>
                              <ImageView fitHeight="34.0" fitWidth="34.0" pickOnBounds="true" preserveRatio="true">
                                 <image>
                                    <Image url="@icons/cluster.png" />
                                 </image>
                              </ImageView>
                           </graphic>
                           <tooltip>
                              <Tooltip text="DX Cluster" />
                           </tooltip></Button>
                     </buttons>
                     <HBox.margin>
                        <Insets right="10.0" />
                     </HBox.margin>
                  </ButtonBar>
               </children>
            </HBox>
            <HBox alignment="CENTER" prefHeight="42.0" GridPane.columnIndex="1" GridPane.valignment="CENTER">
               <children>
                  <Label fx:id="distanceLable" text="Distance to station: " textAlignment="CENTER">
                     <font>
                        <Font name="System Bold" size="17.0" />
                     </font>
                  </Label>
               </children>
            </HBox>
         </children>
      </GridPane>
      <GridPane alignment="TOP_CENTER" gridLinesVisible="true" layoutY="50.0" AnchorPane.bottomAnchor="0.0" AnchorPane.leftAnchor="0.0" AnchorPane.rightAnchor="0.0" AnchorPane.topAnchor="65.0">
         <columnConstraints>
            <ColumnConstraints hgrow="SOMETIMES" minWidth="10.0" />
         </columnConstraints>
         <rowConstraints>
            <RowConstraints minHeight="10.0" vgrow="SOMETIMES" />
         </rowConstraints>
         <children>
              <TableView fx:id="logTableView" editable="true" GridPane.columnSpan="2147483647" GridPane.hgrow="ALWAYS" GridPane.vgrow="ALWAYS">
                  <columns>
                      <TableColumn fx:id="callColumn" maxWidth="500.0" minWidth="200.0" prefWidth="-1.0" text="Callsign" />
                      <TableColumn fx:id="rstColumn" maxWidth="-1.0" minWidth="40.0" prefWidth="-1.0" text="RST" />
                      <TableColumn fx:id="modeColumn" maxWidth="-1.0" minWidth="50.0" prefWidth="-1.0" text="Mode" />
                      <TableColumn maxWidth="-1.0" minWidth="-1.0" prefWidth="-1.0" text="Date / Time">
                          <columns>
                              <TableColumn fx:id="dateColumn" maxWidth="-1.0" minWidth="100.0" prefWidth="-1.0" text="Date" />
                              <TableColumn fx:id="timeColumn" maxWidth="-1.0" minWidth="80.0" prefWidth="-1.0" text="Time" />
                          </columns>
                      </TableColumn>
                      <TableColumn maxWidth="-1.0" minWidth="-1.0" prefWidth="-1.0" text="QSL">
                          <columns>
                              <TableColumn fx:id="eQslRcvdColumn" maxWidth="-1.0" minWidth="70.0" prefWidth="-1.0" text="eQsl" />
                              <TableColumn fx:id="qslRcvdColumn" maxWidth="-1.0" minWidth="70.0" prefWidth="-1.0" text="Bureau" />
                          </columns>
                  </TableColumn>
                      <TableColumn fx:id="freqColumn" maxWidth="-1.0" minWidth="100.0" prefWidth="-1.0" text="Frequency" />
                      <TableColumn fx:id="bandColumn" maxWidth="-1.0" minWidth="50.0" prefWidth="-1.0" text="Band" />
                      <TableColumn fx:id="countryColumn" maxWidth="-1.0" minWidth="200.0" prefWidth="-1.0" text="DXCC Entry" />
                      <TableColumn fx:id="territoryColumn" maxWidth="-1.0" minWidth="200.0" prefWidth="-1.0" text="Territory" />
                  </columns>
               <GridPane.margin>
                  <Insets />
               </GridPane.margin>
               <columnResizePolicy>
                  <TableView fx:constant="CONSTRAINED_RESIZE_POLICY" />
               </columnResizePolicy>
               <contextMenu>
                  <ContextMenu>
                    <items>
                      <MenuItem mnemonicParsing="false" onAction="#handleMoreRecord" text="Add more to this QSO" />
                        <MenuItem mnemonicParsing="false" onAction="#handleEditRow" text="Edit..." />
                        <MenuItem mnemonicParsing="false" onAction="#handleDeleteRows" text="Delete..." />
                        <SeparatorMenuItem mnemonicParsing="false" />
                        <MenuItem mnemonicParsing="false" onAction="#handleShowInfo" text="Get call information" />
                        <MenuItem mnemonicParsing="false" onAction="#handleShowMap" text="Show map" />
                        <MenuItem mnemonicParsing="false" onAction="#handleDistance" text="Distance till station" />
                    </items>
                  </ContextMenu>
               </contextMenu>
              </TableView>
         </children>
      </GridPane>
    </children>
</AnchorPane>
