/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.logger.app.view;

import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.swarl.logger.app.SwarLogMain;
import org.swarl.logger.app.model.SwarLogPreferences;
import org.swarl.logger.app.utils.Alerts;

/**
 * Created by brack on 2017-03-23.
 */
public class PreferencesEditorController {

    @FXML
    private TextField nameField;

    @FXML
    private TextField callField;

    @FXML
    private TextField latField;

    @FXML
    private TextField lonField;

    @FXML
    private TextField eQslCall;

    @FXML
    private PasswordField eQslPass;

    @FXML
    private TextField eQslMessage;

    private Stage dialogStage;
    private SwarLogPreferences preferences;
    private SwarLogMain mainApp;

    @FXML
    private void initialize() {
    }


    public void setDialogStage (Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setMainApp (SwarLogMain mainApp) {
        this.mainApp = mainApp;
    }

    public void setPreferences (SwarLogPreferences preferences) {
        this.preferences = preferences;

        callField.setText(preferences.getMyCall());
        nameField.setText(preferences.getMyName());
        eQslCall.setText(preferences.getMyEQslCall());
        eQslPass.setText(preferences.getMyEQslPass());
        eQslMessage.setText(preferences.getMyEQslMessage());
        latField.setText(preferences.getMyLat());
        lonField.setText(preferences.getMyLon());
    }


    @FXML
    private void handleOk() {
        if (inputIsValid()) {
            preferences.setMyCall(callField.getText());
            preferences.setMyName(nameField.getText());
            preferences.setMyEQslCall(eQslCall.getText());
            preferences.setMyEQslPass(eQslPass.getText());
            preferences.setMyEQslMessage(eQslMessage.getText());
            preferences.setMyLat(latField.getText());
            preferences.setMyLon(lonField.getText());

            mainApp.savePreferences(preferences);
            dialogStage.close();
        } else {
            Alerts.errorAlert(null,null,"Call sign field cannot stay empty");
        }
    }


    @FXML
    private void handleCancel() {
        dialogStage.close();
    }


    private boolean inputIsValid() {
        if (callField.getText() == null) {
            return  false;
        }
        return  true;
    }
}
