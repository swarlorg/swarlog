/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.logger.app.view;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.swarl.brack11.prefixes.model.Prefix;
import org.swarl.logger.app.SwarLogMain;


/**
 * Created by brack on 2017-03-27.
 */
public class PrefixesController {
    private SwarLogMain mainApp;
    private Stage dialogStage;

    @FXML
    private TableView<Prefix> worldPrefixes;

    @FXML
    private TableColumn<Prefix, Double> longitudeColumn;

    @FXML
    private TableColumn<Prefix, Double> latitudeColumn;

    @FXML
    private TableColumn<Prefix, String> territoryColumn;

    @FXML
    private TableColumn<Prefix, String> prefixColumn;

    @FXML
    private TableColumn<Prefix, String> cqColumn;

    @FXML
    private TableColumn<Prefix, String> ituColumn;

    @FXML
    private TableColumn<Prefix, String> continentColumn;

    @FXML
    private TableColumn<Prefix, String> tzColumn;

    @FXML
    private TableColumn<Prefix, String> adifColumn;

    @FXML
    private TableColumn<Prefix, String> provinceColumn;

    @FXML
    private void initialize(){

        longitudeColumn.setCellValueFactory(new PropertyValueFactory<Prefix, Double>("longitude"));
        latitudeColumn.setCellValueFactory(new PropertyValueFactory<Prefix, Double>("latitude"));
        territoryColumn.setCellValueFactory(new PropertyValueFactory<Prefix, String>("territory"));
        prefixColumn.setCellValueFactory(new PropertyValueFactory<Prefix, String>("prefix"));
        cqColumn.setCellValueFactory(new PropertyValueFactory<Prefix, String>("cq"));
        ituColumn.setCellValueFactory(new PropertyValueFactory<Prefix, String>("itu"));
        continentColumn.setCellValueFactory(new PropertyValueFactory<Prefix, String>("continent"));
        tzColumn.setCellValueFactory(new PropertyValueFactory<Prefix, String>("tz"));
        adifColumn.setCellValueFactory(new PropertyValueFactory<Prefix, String>("adif"));
        provinceColumn.setCellValueFactory(new PropertyValueFactory<Prefix, String>("mask"));
    }

    public void setMainApp(SwarLogMain mainApp) {
        this.mainApp = mainApp;

        ObservableList<Prefix> prefixes = mainApp.getPrefixes().getRecords();

        worldPrefixes.setItems(prefixes);

    }


    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

}
