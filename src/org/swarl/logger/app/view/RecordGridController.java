/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.logger.app.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import org.swarl.brack11.adif2.model.Adif2Record;
import org.swarl.brack11.adif2.utils.DateTimeWrapper;
import org.swarl.brack11.prefixes.model.Call;
import org.swarl.brack11.prefixes.model.Prefix;
import org.swarl.brack11.prefixes.model.TreeNode;

import java.time.format.DateTimeFormatter;

/**
 * Created by Yury Bondarenko on 2017-04-06.
 * <p>
 * Owner: Yury Bondarenko
 * Website: http://www.swarl.org
 * Email: this.brack@gmail.com
 */
public class RecordGridController {
    @FXML
    private TextField call;

    @FXML
    private TextField callC;

    @FXML
    private TextField rst;

    @FXML
    private TextField rstC;

    @FXML
    private TextArea note;

    @FXML
    private TextArea noteC;

    @FXML
    private TextField utc;

    @FXML
    private Button addRow;

    @FXML
    private Button removeRow;

    @FXML
    private ContextMenu callContext;

    @FXML
    private ContextMenu callCContext;

    private AddLogRecordController superController;

    @FXML
    public void initialize(AddLogRecordController superController) {
        this.superController = superController;
        call.textProperty().addListener((ov, oldValue, newValue) -> {
            call.setText(newValue.toUpperCase());
        });
        callC.textProperty().addListener((ov, oldValue, newValue) -> {
            callC.setText(newValue.toUpperCase());
        });
    }


    @FXML
    private void addRowHandler() {
        superController.addRowHandle();
    }

    @FXML
    private void removeRowHandler(ActionEvent event) {
        superController.removeRowHandle(event);
    }

    @FXML
    private void hearWell(MouseEvent event) {
        if (event.getClickCount() == 2) {
            TextField field = (TextField) event.getSource();
            field.setText("599");
        }
    }

    @FXML
    private void utcNow(MouseEvent event) {
        if (event.getClickCount() == 2) {
            TextField field = (TextField) event.getSource();
            field.setText(DateTimeWrapper.nowUtcTime());
        }
    }

    @FXML
    private void handleShowInfo() {
        if (!call.getText().isEmpty()) {
            superController.getMainApp().showWeb("https://www.qrzcq.com/call/" + call.getText());
        }
    }

    @FXML
    private void handleShowInfoC() {
        if (!callC.getText().isEmpty()) {
            superController.getMainApp().showWeb("https://www.qrzcq.com/call/" + callC.getText());
        }
    }

    public void styleFirstRow(boolean firstRow) {
        if (firstRow) {
            call.setStyle("-fx-min-height: 50; -fx-font-size: 25; -fx-font-weight: bold; -fx-background-color: CCE4B6;");
            callC.setStyle("-fx-min-height: 50; -fx-font-size: 25; -fx-font-weight: bold; -fx-background-color: CCE4B6;");
            utc.setStyle("-fx-background-color: CCE4B6;");
            rst.setStyle("-fx-background-color: CCE4B6;");
        } else {
            call.setStyle("-fx-min-height: 50; -fx-font-weight: bold; -fx-font-size: 25;");
            callC.setStyle("-fx-min-height: 50; -fx-font-weight: bold; -fx-font-size: 25;");
        }
    }

    public void setLastRecord(Adif2Record lastRecord) {
        if (lastRecord != null) {
            String rstString = (lastRecord.getRstRcvd() != null) ? lastRecord.getRstRcvd() : lastRecord.getRstSent();
            call.setText(lastRecord.getCall());
            rst.setText(rstString);
            utc.setText(lastRecord.getTimeOn().format(DateTimeFormatter.ofPattern("HH:mm")));
            note.setText(lastRecord.getNotes());
        }
    }


    public ObservableMap<String, String> getRecordData() {
        ObservableMap<String, String> recordData = FXCollections.observableHashMap();
        addData(recordData,"call", call.getText());
        addData(recordData,"callC", callC.getText());
        addData(recordData,"rst", rst.getText());
        addData(recordData,"rstC", rstC.getText());
        addData(recordData,"note", note.getText());
        addData(recordData,"noteC", noteC.getText());
        addData(recordData,"utc", utc.getText());

        return recordData;
    }


    public void hideRemoveButton() {
        removeRow.disarm();
        removeRow.setStyle("visibility: hidden");
    }

    public boolean isValid() {
        if (call.getText() == null || call.getText().length() < 1) {
            return false;
        }
        if (callC.getText() == null || callC.getText().length() < 1) {
            return false;
        }
        if (rst.getText() == null || rst.getText().length() < 2) {
            return false;
        }
        if (utc.getText() == null || utc.getText().length() < 2) {
            return false;
        }
        return true;
    }

    private void addData(ObservableMap<String, String> record, String key, String value) {
        if ((value != null) && (!value.isEmpty())) {
            record.put(key, value.toUpperCase());
        }
    }

    private ObservableList<MenuItem> allMenuItems(String callStr) {
        ObservableList<MenuItem> menuItems = FXCollections.observableArrayList();
        Adif2Record aRecord = new Adif2Record();
        Call call = new Call(callStr);
        TreeNode<Prefix> prefixTree = call.prefixTreeOf(superController.getMainApp().getPrefixes());
        if (prefixTree != null) {
            MenuItem country = new MenuItem(aRecord.getCountry());
            country.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    superController.getMainApp().showWeb("https://www.google.com/maps/place/" + aRecord.getLat().toString() + "+" + aRecord.getLon().toString());
                }
            });
            MenuItem territory = new MenuItem(aRecord.getAppSwarlogTerritory());
            territory.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    superController.getMainApp().showWeb("https://www.google.com/maps/place/" + aRecord.getLat().toString() + "+" + aRecord.getLon().toString());
                }
            });
            menuItems.addAll(country, territory);
        }
        return menuItems;
    }
}
