/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.logger.app.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.swarl.brack11.adif2.Adif2Writer;
import org.swarl.brack11.adif2.enums.Band;
import org.swarl.brack11.adif2.enums.Mode;
import org.swarl.brack11.adif2.enums.QSLRcvd;
import org.swarl.brack11.adif2.model.Adif2Record;
import org.swarl.brack11.adif2.utils.DateTimeWrapper;
import org.swarl.brack11.location.Location;
import org.swarl.brack11.location.model.Coordinate;
import org.swarl.brack11.prefixes.enums.PrefixKind;
import org.swarl.brack11.prefixes.model.Call;
import org.swarl.brack11.prefixes.model.Prefix;
import org.swarl.brack11.prefixes.model.TreeNode;
import org.swarl.logger.app.SwarLogMain;
import org.swarl.logger.app.utils.Alerts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Created by Yury Bondarenko on 2017-03-30.
 * <p>
 * Owner: Yury Bondarenko
 * Website: http://www.swarl.org
 * Email: this.brack@gmail.com
 */
public class AddLogRecordController {
    @FXML
    private GridPane mainGrid;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private DatePicker qsoDate;

    @FXML
    private TextField qsoFreq;

    @FXML
    private ComboBox<Mode> qsoMode;

    @FXML
    private Button submitQso;

    private SwarLogMain mainApp;
    private Stage dialogStage;
    private int rowIndex;
    //private Map<AnchorPane, RecordGridController> controllers;
    private Map<Integer, Map<AnchorPane, RecordGridController>> controllers;
    private Band qsoBand;
    private EnumMap<Mode, Boolean> modeMap;


    @FXML
    public void initialize(SwarLogMain mainApp, Adif2Record lastRecord, boolean addMore) {
        rowIndex = 0;
        this.mainApp = mainApp;
        //controllers = new HashMap<>();
        controllers = new HashMap<>();

        modeMap = mainApp.readUsedModes();
        ObservableList<Mode> modes = FXCollections.observableArrayList(modeMap.entrySet()
                .stream()
                .filter(e -> e.getValue())
                .map(map -> map.getKey())
                .collect(Collectors.toList())
        );
        qsoMode.getItems().addAll(modes);
        qsoMode.setCellFactory(new Callback<ListView<Mode>, ListCell<Mode>>() {
            @Override
            public ListCell<Mode> call(ListView<Mode> param) {
                return new ListCell<Mode>() {
                    @Override
                    protected void updateItem(Mode item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            if (Mode.isRoot(item)) {
                                setTextFill(Color.BLACK);
                                setText(item.toString());
                            } else {
                                setTextFill(Color.BLUE);
                                setText("   " + item.toString());
                            }
                        }
                    }
                };
            }
        });
        qsoMode.setValue(lastRecord != null && lastRecord.getMode() != null ? setMode(lastRecord.getMode()) : setMode(Mode.SSB));

        if (lastRecord != null) {
            if (lastRecord.getFreqRx() == 0) {
                qsoFreq.setText(lastRecord.getFreq() != null ? lastRecord.getFreq().toString() : "");
            } else {
                qsoFreq.setText(lastRecord.getFreqRx() != null ? lastRecord.getFreqRx().toString() : "");
            }
        }

        if (addMore && lastRecord != null) {
            qsoDate.setValue(lastRecord.getQsoDate());
            loadRecordGrid(lastRecord);
        } else {
            qsoDate.setValue(LocalDate.now(ZoneId.of("UTC")));
            loadRecordGrid(null);
        }
    }

    private Mode setMode(Mode mode) {
        if (!modeMap.get(mode)) {
            modeMap.replace(mode, true);
            //mainApp.writeUsedModes(modeMap);
        }
        return mode;
    }


    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    @FXML
    private void handleSubmit() {
        if (generalIsValid()) {
            submitQso.disableProperty();
            String dateFormated = DateTimeWrapper.toAdifDate(qsoDate.getValue());
            String filePath = mainApp.getAdifFilePath().getPath();
            String freq = qsoFreq.getText();
            qsoBand = Band.getByFreq(Double.parseDouble(freq));

            if (controllers.get(1).entrySet().iterator().next().getValue().isValid()) {

                Adif2Writer writer = new Adif2Writer();
                for (Map.Entry<Integer, Map<AnchorPane, RecordGridController>> entry : controllers.entrySet()) {
                    processQSOs(dateFormated, filePath, freq, qsoBand, entry.getValue().entrySet().iterator().next(), writer, '<');
                    processQSOs(dateFormated, filePath, freq, qsoBand, entry.getValue().entrySet().iterator().next(), writer, '>');
                }

                File file = mainApp.getAdifFilePath();
                mainApp.loadDataFromFile(file);
                dialogStage.close();
            } else {
                Alerts.errorAlert(null,null,"fields CALL 1, CALL 2, RST 1, UTC should be filled at least on the first row.");
            }
        } else {
            Hyperlink link = new Hyperlink("Band Enumeration");
            link.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    mainApp.showWeb("http://www.adif.org/307/ADIF_307.htm#Band_Enumeration");
                }
            });
            Alert error = new Alert(Alert.AlertType.ERROR);
            error.setHeaderText(null);
            error.getDialogPane().setContent(new TextFlow(
                    new Text("Date, Mode and Frequency cannot be empty \nand Frequecy must be compliant with ADIF "),
                    link
            ));
            error.show();
        }
    }

    private boolean generalIsValid() {
        if (qsoFreq.getText() == null || qsoFreq.getText().length() < 2) {
            return false;
        } else if (Band.kHzTomHz(Double.parseDouble(qsoFreq.getText())) == 0) {
            return false;
        }
        if (qsoMode.getValue() == null) {
            return false;
        }
        if (qsoDate.getValue() == null) {
            return false;
        }
        return true;
    }

    private void processQSOs(String dateFormated,
                             String filePath,
                             String freq,
                             Band qsoBand,
                             Map.Entry<AnchorPane, RecordGridController> entry,
                             Adif2Writer writer,
                             char side) {
        String s = (side == '<') ? "" : "C";
        RecordGridController gridController = entry.getValue();
        Map<String, String> recordData = gridController.getRecordData();
        if ((recordData.get("call" + s) != null)
                && (recordData.get("rst" + s) != null)
                && (recordData.get("rst" + s).length() > 1)) {
            Adif2Record record = new Adif2Record();
            Call callSign = new Call(recordData.get("call" + s));
            TreeNode<Prefix> prefixTree = callSign.prefixTreeOf(mainApp.getPrefixes());
            String utc = recordData.getOrDefault("utc", DateTimeWrapper.nowUtcTime());
            String utcFormatted = DateTimeWrapper.toAdifTime(LocalTime.parse(utc));
            String note = recordData.getOrDefault("note" + s, "");
            String rst = recordData.getOrDefault("rst" + s, "");
            String qsoMsg = qsoMsg(side);
            record.setCall(callSign.getCall());
            record.setFreq(freq);
            record.setBand(qsoBand);
            record.setQsoDate(dateFormated);
            record.setMode(qsoMode.getValue());
            record.setTimeOn(utcFormatted);
            if (!note.isEmpty()) record.setNotes(note);
            if (!rst.isEmpty()) {
                record.setRstRcvd(rst);
                record.setRstSent(rst);
            }
            record.setQslmsg(qsoMsg);
            record.setEqslQslRcvd(QSLRcvd.NO);
            record.setQslRcvd(QSLRcvd.NO);

            addPrefixInfo(prefixTree, record);

            writer.write(record, filePath);
        }
    }

    protected void addPrefixInfo(TreeNode<Prefix> prefixTree, Adif2Record record) {
        try {
            for (TreeNode<Prefix> node : prefixTree) {
                if (node.isRoot()) {
                    record.setCont(node.data.getContinent());
                    record.setCountry(node.data.getTerritory());
                }
                if (node.isLeaf() && (node.data.getType().equals(PrefixKind.DXCC) || node.data.getType().equals(PrefixKind.CITY) || node.data.getType().equals(PrefixKind.PROVINCE))) {
                    record.setAppSwarlogTerritory(node.data.getTerritory());
                }
                String gridSquare = Location.latLonToGridSquare((float)node.data.getLatitude(),(float)node.data.getLongitude(),4);
                record.setGridsquare(gridSquare);
                record.setCqz(node.data.getCq());
                record.setItuz(node.data.getItu());
                record.setLat(new Coordinate("lat",node.data.getLatitude()));
                record.setLon(new Coordinate("lon",node.data.getLongitude()));
                record.setPfx(node.data.getPrefix());
                record.setDxcc(node.data.getAdif());
                record.setAppSwarlogTz(node.data.getTz());
            }
        } catch (Exception e) {
            //Alerts.exceptionAlert(e, "Cannot get prefix information for " + record.getCall());
            Alerts.errorAlert("","","Illegal call sign form");
            e.printStackTrace();
        }

    }

    /**
     * Forming qso message
     *
     * @param c can be one of two values '<' for forming message for left part or '>' for right side of qso
     * @return string message
     */
    private String qsoMsg(char c) {
        StringBuilder sb = new StringBuilder();
        String message = mainApp.getPreferences().getMyEQslMessage();
        sb.append(message);
        //for (Map.Entry<AnchorPane, RecordGridController> entry : controllers.entrySet()) {
        for (Map.Entry<Integer, Map<AnchorPane, RecordGridController>> entry : controllers.entrySet()) {
            RecordGridController gridController = entry.getValue().entrySet().iterator().next().getValue();
            Map<String, String> m = gridController.getRecordData();
            switch (c) {
                case '>':
                    sb.append(" " + m.getOrDefault("call", ""));
                    break;
                case '<':
                    sb.append(" " + m.getOrDefault("callC", ""));
                    break;
                default:
                    return null;
            }
        }
        return sb.toString().toUpperCase();
    }

    /**
     * Handles removal of not needed grid
     *
     * @param event Passed from button click
     */
    public void removeRowHandle(ActionEvent event) {
        Parent btn = (Parent) event.getSource();
        Parent pane = btn.getParent().getParent();

        int row = (int) pane.getProperties().get("gridpane-row");
        if(row > 0) {
            mainGrid.getChildren().remove(pane);
            controllers.remove(pane);
        }
    }

    /**
     * Handle new record button on each of the grids
     */
    public void addRowHandle() {
        loadRecordGrid(null);
        scrollPane.vvalueProperty().bind(mainGrid.heightProperty());
    }


    /**
     * Creates new grid out of RecordGrid.fxml
     */
    private void loadRecordGrid(Adif2Record lastRecord) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("RecordGrid.fxml"));

            AnchorPane pane = loader.load();
            mainGrid.add(pane, 0, rowIndex++);
            RecordGridController controller = (RecordGridController) loader.getController();
            controller.initialize(this);

            controller.setLastRecord(lastRecord);

            Map<AnchorPane, RecordGridController> controllerMap = new HashMap<>();
            controllerMap.put(pane, controller);

            controllers.put(rowIndex, controllerMap);
//            controllers.add(rowIndex++, controller);

            if (rowIndex == 1) {
                controller.hideRemoveButton();
                controller.styleFirstRow(true);
            } else {
                controller.styleFirstRow(false);
            }

        } catch (IOException e) {
            Alerts.exceptionAlert(e, "Exception thrown: ");
        }
    }

    public SwarLogMain getMainApp() {
        return mainApp;
    }

}
