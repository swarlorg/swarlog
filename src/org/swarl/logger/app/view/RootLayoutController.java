/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.logger.app.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.swarl.logger.app.SwarLogMain;
import org.swarl.logger.app.model.SwarLogPreferences;
import org.swarl.logger.app.utils.Alerts;

import java.io.File;
import java.io.IOException;

public class RootLayoutController {

    private SwarLogMain mainApp;
    private SwarLogPreferences preferences;


    /**
     * Referrance to the main class
     * @param mainApp SwarLogMain main application class
     */
    public void setMainApp(SwarLogMain mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * Action on press of button Open
     */
    @FXML
    private void handleOpen () {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new ExtensionFilter("ADIF files (*.adi)", "*.adi"));
        File file = fileChooser.showOpenDialog(mainApp.getPrimaryStage());

        if (file != null) {
            mainApp.loadDataFromFile(file);
        }
    }

    @FXML
    private void handleImport() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new ExtensionFilter("ADIF files (*.adi)", "*.adi"));
        File file = fileChooser.showOpenDialog(mainApp.getPrimaryStage());

        if (file != null) {
            mainApp.importDataFromFile(file);
        }
    }

    @FXML
    private void handleSave() {
        mainApp.saveToAdiFile();
    }

    /**
     * Action on press of button File->Close
     */
    @FXML
    private void handleExit() {
        System.exit(0);
    }

    /**
     * Action on press of button Edit->SwarLogPreferences
     */
    @FXML
    private void handlePreferences() {
        mainApp.showPreferencesDialog();
    }

    /**
     * Action on press of button About
     */
    //TODO keep versioning application
    @FXML
    private void handleAbout() {
        String version = "2.8.2";
        Hyperlink website = new Hyperlink("http://swarlog.sourceforge.io");
        Hyperlink source = new Hyperlink("https://bitbucket.org/swarlorg/");
        Hyperlink changeLog = new Hyperlink("https://bitbucket.org/swarlorg/swarlog");
        Hyperlink prefixes = new Hyperlink("http://www.dxatlas.com");
        website.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mainApp.showWeb("http://swarlog.sourceforge.io");
            }
        });
        source.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mainApp.showWeb("https://bitbucket.org/swarlorg/");
            }
        });
        changeLog.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mainApp.showWeb("https://bitbucket.org/swarlorg/swarlog");
            }
        });
        prefixes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mainApp.showWeb("http://www.dxatlas.com");
            }
        });
        TextFlow flow = new TextFlow(
                new Text("Author: Yury Bondarenko "),
                new Text("\nWebsite: "), website,
                new Text("\nSource code: "), source,
                new Text("\nChangelog: "), changeLog,
                new Text("\nPrefix database source: "), prefixes
        );
        Alert about = new Alert(Alert.AlertType.INFORMATION);
        about.setTitle("SWARLog");
        about.setHeaderText("About version: " + version);
        about.getDialogPane().setContent(flow);
        about.show();
    }


    /**
     * Loading prefixes dialog
     */
    @FXML
    private void handleFreq() {
        //mainApp.showPrefixesDialog();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ModesUsed.fxml"));

            AnchorPane frequenciesDialog = loader.load();

            // Create dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Used Modes");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setWidth(300);
            Scene scene = new Scene(frequenciesDialog);
            dialogStage.setScene(scene);

            ModesUsedController controller = loader.getController();
            controller.setMainApp(mainApp);

            //controller.setDialogStage(dialogStage);

            dialogStage.show();
        } catch (IOException e) {
            Alerts.exceptionAlert(e, "Exception thrown: ");
        }

    }


    /**
     * Send information to eQSL.cc
     */
    @FXML
    private void handleEQslSend() {
        mainApp.eQslSend();
    }

    /**
     * Receive information from eQsl.cc
     */
    @FXML
    private void handleEQslReceive() {
        try {
            mainApp.eQslReceive();
        } catch (Exception e) {
            Alerts.exceptionAlert(e, "Error: ");
        }
    }


    /**
     * Opens DX Cluster in default browser
     */
    @FXML
    private void handleDxCluster() {
        mainApp.showWeb("http://www.dxsummit.fi/#/");
    }

    @FXML
    private void handleImportAdif() {
        //TODO Import Adif File
    }

    public void setPreferences(SwarLogPreferences preferences) {
        this.preferences = preferences;
    }
}
