/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.logger.app.view;

import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.Callback;
import org.controlsfx.control.table.TableFilter;
import org.swarl.brack11.adif2.enums.Band;
import org.swarl.brack11.adif2.enums.Mode;
import org.swarl.brack11.adif2.enums.QSLRcvd;
import org.swarl.brack11.adif2.model.Adif2Record;
import org.swarl.brack11.location.model.Coordinate;
import org.swarl.logger.app.SwarLogMain;
import org.swarl.logger.app.utils.Alerts;
import org.swarl.logger.app.utils.DistanceCalculator;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;


/**
 * Created by Yury Bondarenko on 2017-02-24.
 */
public class LoggerLayoutController {

    //TODO make editable table view
    //TODO make column with delete buttons
    @FXML
    private TableView<Adif2Record> logTableView;

    @FXML
    private Label distanceLable;

    @FXML
    private TableColumn<Adif2Record, String> callColumn;

    @FXML
    private TableColumn<Adif2Record, String> rstColumn;

    @FXML
    private TableColumn<Adif2Record, Mode> modeColumn;

    @FXML
    private TableColumn<Adif2Record, LocalDate> dateColumn;

    @FXML
    private TableColumn<Adif2Record, LocalTime> timeColumn;

    @FXML
    private TableColumn<Adif2Record, Double> freqColumn;

    @FXML
    private TableColumn<Adif2Record, Band> bandColumn;

    @FXML
    private TableColumn<Adif2Record, String> countryColumn;

    @FXML
    private TableColumn<Adif2Record, String> territoryColumn;

    @FXML
    private TableColumn<Adif2Record, QSLRcvd> eQslRcvdColumn;

    @FXML
    private TableColumn<Adif2Record, QSLRcvd> qslRcvdColumn;

    private SwarLogMain mainApp;

    private Adif2Record record;

    /**
     * Reference to the main class
     * @param mainApp SwarLogMain main application class
     */
    public void setMainApp(SwarLogMain mainApp) {
        this.mainApp = mainApp;

        tableSetup();
        emptyPrefs();
    }

    /**
     * TableView setup
     */
    private void tableSetup() {
        logTableView.setItems(mainApp.getRecords());
        logTableView.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE
        );

        logTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                record = newSelection;
                distanceLable.setText(handleDistance(newSelection));
            }
        });

        TableFilter<Adif2Record> filter = TableFilter.forTableView(logTableView).lazy(false).apply();
        filter.setSearchStrategy((input, target) -> {
            try {
                return target.toLowerCase().contains(input.toLowerCase());
            } catch (Exception e) {
                return false;
            }
        });
    }

    private void emptyPrefs() {
        if (mainApp.getPreferences() == null) {
            handlePreferences();
        }
    }

    @FXML
    private void initialize() {
        distanceLable.setText("");
        callColumn.setCellValueFactory(c->c.getValue().callProperty());

        // Shows RST_RCVD value by default or RST_SENT if RST_RCVD is empty
        rstColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Adif2Record, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Adif2Record, String> param) {
                Adif2Record adif = param.getValue();
                if ((adif.getRstRcvd() == null) || (adif.getRstRcvd().trim().isEmpty())) {
                    return adif.rstSentProperty();
                }
                return adif.rstRcvdProperty();
            }
        });

        modeColumn.setCellValueFactory(c->c.getValue().modeProperty());
        dateColumn.setCellValueFactory(c->c.getValue().qsoDateProperty());
        timeColumn.setCellValueFactory(c->c.getValue().timeOnProperty());
        qslRcvdColumn.setCellValueFactory(c->c.getValue().qslRcvdProperty());
        eQslRcvdColumn.setCellValueFactory(c->c.getValue().eQslQslRcvdProperty());
        freqColumn.setCellValueFactory(c->c.getValue().freqProperty().asObject());

        // Shows bandRx if it's not empty, otherwise band
        bandColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Adif2Record, Band>, ObservableValue<Band>>() {
            @Override
            public ObservableValue<Band> call(TableColumn.CellDataFeatures<Adif2Record, Band> param) {
                Adif2Record adif = param.getValue();
                if ((adif.getBandRx() == null) || (adif.getBandRx().length() == 0)) {
                    return adif.bandProperty();
                }
                return adif.bandRxProperty();
            }
        });

        countryColumn.setCellValueFactory(c->c.getValue().countryProperty());
        territoryColumn.setCellValueFactory(c->c.getValue().appSwarlogTerritoryProperty());
    }

    @FXML
    private void handleNewRecord() {
        mainApp.showAddLogDialog();
    }

    @FXML
    private void handleShowInfo() {
        if (record != null) {
            mainApp.showWeb("https://www.qrzcq.com/call/" + record.getCall());
        }
    }

    @FXML
    private void handleShowMap() {
        if (record != null) {
            mainApp.showWeb("https://www.google.com/maps/place/" + record.getLat().toString() + "+" + record.getLon().toString());
        }
    }

    @FXML
    private void handleMoreRecord() {
        if (record == null) {
            Alerts.warningAlert("No selection", null, "For this button to work \nyou must select log entry \nthat you want to extend or add to.");
        } else {
            mainApp.showAddLogDialog(record);
        }
    }

    @FXML
    private void handleDeleteRows() {
        ObservableList<Adif2Record> selectedRows = logTableView.getSelectionModel().getSelectedItems();
        if (selectedRows.size() > 0) {
            Optional<ButtonType> result = Alerts.confirmationAlert("Delete Entries", null, "Confirm deleting selected log entries. Changes cannot be reverted.");

            if (!result.get().equals(ButtonType.CANCEL)) {
                //logTableView.getItems().removeAll(selectedRows);
                mainApp.getRecords().removeAll(selectedRows);
                mainApp.writeChangesTo();
                logTableView.getSelectionModel().clearSelection();
            }

        } else {
            //Nothing selected
            Alerts.errorAlert("No selection", null, "Please select some records in the table.");
        }
    }


    @FXML
    private void handleEditRow() {
        if (record != null) {
            mainApp.showEditDialog(record);
        } else {
            Alerts.errorAlert("No selection", null, "Please select some records in the table.");
        }
    }

    /**
     * Action on press of button Preferences
     */
    @FXML
    private void handlePreferences() {
        mainApp.showPreferencesDialog();
    }

    /**
     * Opens DX Cluster in default browser
     */
    @FXML
    private void handleDxCluster() {
        mainApp.showWeb("http://www.dxsummit.fi/#/");
    }


    @FXML
    private void handleDistance() {
        if (mainApp.getPreferences().getMyLat() != null &&
                mainApp.getPreferences().getMyLon() != null &&
                mainApp.getPreferences().getMyLat().length() > 0 &&
                mainApp.getPreferences().getMyLon().length() > 0) {
            if (record.getLat() != null &&
                    record.getLon() != null) {
                Double lat1 = Double.parseDouble(mainApp.getPreferences().getMyLat());
                Double lon1 = Double.parseDouble(mainApp.getPreferences().getMyLon());
                Double lat2 = Coordinate.dmToLat(record.getLat().toString());
                Double lon2 = Coordinate.dmToLon(record.getLon().toString());

                String distance = DistanceCalculator.distance(lat1, lon1, lat2, lon2, "K");

                Alerts.infoAlert("Distance", null, "Distance between " + mainApp.getPreferences().getMyCall() + " and " + record.getCall() + " is " + distance);
            } else {
                Alerts.infoAlert("Distance", null, "Distance between " + mainApp.getPreferences().getMyCall() + " and " + record.getCall() + " cannot be calculated since " + record.getCall() + " doesnt have valid coordinates");
            }
        } else {
            Alerts.errorAlert("Distance", "No information about your location.", "Set your coordinates in preferences.");
        }
    }

    private String handleDistance(Adif2Record record) {
        String result = "Insert your coordinates in preferences";
        if (mainApp.getPreferences() != null &&
                mainApp.getPreferences().getMyLat() != null &&
                mainApp.getPreferences().getMyLon() != null &&
                mainApp.getPreferences().getMyLat().length() > 0 &&
                mainApp.getPreferences().getMyLon().length() > 0) {
            if (record.getLat() != null &&
                record.getLon() != null) {
                Double lat1 = Double.parseDouble(mainApp.getPreferences().getMyLat());
                Double lon1 = Double.parseDouble(mainApp.getPreferences().getMyLon());
                Double lat2 = Coordinate.dmToLat(record.getLat().toString());
                Double lon2 = Coordinate.dmToLon(record.getLon().toString());

                result = "Till selected station: " + DistanceCalculator.distance(lat1, lon1, lat2, lon2, "K");
            } else {
                result = "Unknown location. Insert coordinates for call";
            }
        }
        return result;
    }
}
