# README #

### What is this repository for? ###

Java 1.8 based project designed for logging QSOs by ham radio listeners (SWL)
This logger differs from others by it's unique form of addin QSOs as well as direct connection (in future versions) with
web QSL service http://eQSL.cc
All records are saved directly to .adi ADIF files compatible with eQSL.cc API

### Changes ###
#### v1.3.4.1 ####
* style of preferences window changed to **UNDECORATED**
#### v1.3.5 ####
* Fixed exceptions while trying to submit empty Log entry
* Colorized fields in addin log form to signify required fields
#### v1.3.6 ####
* Restyling form for adding log entry
* Solved issue when application cannot find it's records in registry (during first start)
#### v1.4 ####
* Optimizing code
* Add QSL message text configuration in preferences
#### v1.4.1 ####
* Minor fixes and optimizations
* Only first row of multirowed QSO is validated
* Source of Prefix.lst file add to About
#### v1.5 ####
* Optimizing validation of the Edit form
* Minor fixes
#### v1.6 ####
* Minor fixes and optimizations
* Shift to the last line while add new correspondent in QSO
* Default frequency is in mHz
#### v2.0 ####
* Bands are converted in accordance with ADIF 3.07
* Frequencies automatically converted to mHz
* Inserted valid hyperlinks in some alerts
* Minor fixes
#### v2.3 ####
* Import ADIF file functionality
* Minor fixes and optimizations
#### v2.4 ####
* Ability to select used Modes
#### v2.5 ####
* Minor fixes
* Change of handling eQSL.cc check (fixed nullpointer exception when no entries to update)
#### v2.6 ####
* changed font styles in window of add new QSO
#### v2.7 ####
* Add content menu to call fields in add qso window
#### v2.8 ####
* Code optimizing

### Contribution guidelines ###

Code is licensed under Apache license version 2 http://www.apache.org/licenses/LICENSE-2.0

Code can be freely used by any project

**_This project can be very large in future and I would appreciate joining contributors. Any help would be welcomed._**

### Who do I talk to? ###

* Owner Yury Bondarenko
* Email this.brack@gmail.com